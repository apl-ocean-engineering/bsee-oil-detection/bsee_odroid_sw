# Camera calibrations

## phoenix_234200113.yaml

Intrinsic calibration of Phoenix camera s/n 234200113 (conventional RJ45 ethernet connector), which was used on the Raven ROV at Ohmsett in June 2024.   It was calibrated in the HH101 tank on Friday June 21st **after** Ohmsett.  Data in `bsee_data` [here](https://gitlab.com/apl-ocean-engineering/bsee-oil-detection/bsee_camera_analysis/-/tree/main/2024-06-21_raven_bsee_camera_calibration?ref_type=heads) and calibration workspace [here](https://gitlab.com/apl-ocean-engineering/bsee-oil-detection/bsee_data/-/tree/main/post-OHMSETT-2024/240621_195939_sled_cam_calibration?ref_type=heads).
