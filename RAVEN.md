# Running the BSEE sled on Raven

`raven-ros` is the ros master.

## On raven-ros

BSEE-specific configuration is in the directory `~sysop/bsee/bsee_ws`.    The alias `bsee` will source that workspace:

```bash
> bsee
```

Then run a BSEE-specific version of the standard Raven launchfile:

```bash
> roslaunch bsee_odroid_sw raven.launch
```



## On bsee-rov

SSH into bsee-rov:

```bash
> ssh bsee-rov
```

The alias `raven` will configure `ROS_MASTER_URI` and source the `~bsee/bsee_ws` workspace.  Then the `raven_sled.launch` launchfile will start all service.  The `mission` param must be provided:

```bash
> raven
> roslaunch bsee_odroid_sw raven_sled.launch mission:=mission###
```

For testing at OSB/bench, use "mission:=osb" or "mission:=bench"; dates will be automatically added.
For OHMSETT, follow CRREL pattern of mission001 ...

## on raven-ros

rqt_bsee_power # opens up power control widget + power/HTP plots
rqt_bsee_science  # plots par and uvilux
rqt_bsee_camera  # ...
