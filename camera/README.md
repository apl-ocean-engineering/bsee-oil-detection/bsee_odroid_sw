# Lucidvision / Arena camera driver for BSEE

Communication with the [Lucidvision](https://thinklucid.com/) machine vision camera is handled by their "Arena" SDK.  To encapsulate this dependency, the CI job for [arena_camera_ros](https://github.com/apl-ocean-engineering/arena_camera_ros) automatically builds a [Docker container](https://github.com/apl-ocean-engineering/arena_camera_ros/pkgs/container/arena_camera_ros) which includes the Arena SDK, and a ROS workspace which includes the `arena_camera_ros` node.

For BSEE we extend this Docker image to create a custom Docker image which includes additional dependencies and default configurations.

The preferred mode of operation is for **this directory** to be mounted in the Docker image, and for the camera driver to be launched from the [`bsee_camera_nodelet.launch`](bsee_camera_nodelet.launch) launchfile, which will load rosparams from the yaml files also in this directory.   This allows rapid updates of the launchfile and default params without rebuilding the docker image.

For convenience, the [`start_camera_in_docker.sh`](start_camera_in_docker.sh) script wraps the process of launching the Docker image with the volume mount.   It calls `roslaunch bsee_camera_nodelet.launch`

`start_camera_in_docker.sh` can also be called from a launchfile:

```
  <node name="camera_launcher" pkg="bsee_odroid_sw"              type="start_camera_in_docker.sh"
      required="true"
      output="screen" if="$(arg enable_camera)" args="$(arg which_camera)"/>
```

**however**, since it is chaining out to another instance of `roslaunch` it does not respect namespacing and args from the parent launchfile.   These must be changed in [bsee_camera_nodelet.launch](bsee_camera_nodelet.launch)

# Pulling the Docker image.

The Gitlab CI should maintain an up-to-date version of the `main` branch.  This can be pulled with:

```
docker pull registry.gitlab.com/apl-ocean-engineering/bsee-oil-detection/bsee_odroid_sw/camera:latest
```

or

```
docker compose pull
```


# Building the Docker image.

If needed, the Docker image can be built locally.  As CI only builds `main` branch, local builds are required when working with branches.  To build:

```
docker compose build
```

This process requires the [arena_camera_ros:latest]() docker image. Docker compose will try to download it if required.  It also pulls repos from gitlab (e.g. it will not work offline).
