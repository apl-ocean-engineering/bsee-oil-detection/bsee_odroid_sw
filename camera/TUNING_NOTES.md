# Interrupt tuning

On the Odroid C4 could not move the ethernet adaptor off of CPU0 (why not), so moved everything else away:

| IRQ | Function |
|-----|----------|
| 21 | eth0 |
| 22 | xhci-hcd:usb1 |
| 27 | ffd1d000.i2c |
| 28 | ffd1c000.i2c |
| 49 | vsync, osd-vsync |
| 57 | rdma |
| 58 | meson-aml-mmc |

Can observe the number of interrupts by `cat /proc/interrupts` or:

```
watch -d1 -n cat /proc/interrupts
```

# UDP Tuning

Can observe network statistic with `netstat` or `nstat`:

```
watch -d1 -n sh -c "sh -c nstat | grep -i udp"
```

(nstat resets the counters every time it is called...)


```
watch -d1 -n  ss -ulm
```

Shows queue lengths and memory statistics for each UDP listening socket


Current tuning with sysctl:

```
# Allow 25MB of receive buffer memory
net.core.rmem_max=52428800
net.core.rmem_default=26214400
net.core.netdev_max_backlog=2000
```
