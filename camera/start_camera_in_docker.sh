#!/bin/bash
#
# This is the "userspace" entrypoint for running the Arena camera docker.
# It mounts the bsee_odroid_sw/camera/ directory (this directory) in the Docker image
# and executes the docker_entrypoint.sh script

if [[ $1 == "triton" ]]; then
        CAMERA="triton"
        HDR_IMAGE_PROC="true"
elif [[ $1 == "phoenix" ]]; then
        CAMERA="phoenix"
        HDR_IMAGE_PROC="false"
else
        echo "Usage: $0 [phoenix|triton]"
        exit 0
fi

CAMERA_NAMESPACE=${CAMERA_NAMESPACE:-}

SCRIPT=$(readlink -f "$0")
BASEDIR=$(dirname "$SCRIPT")
CAMERA_DIR=$BASEDIR

echo "Loading launchfiles from $CAMERA_DIR"

if [[ ! -d $CAMERA_DIR ]]; then
        echo "Hm, couldn't find the directory $CAMERA_DIR"
        exit -1
fi

docker run -ti --net host \
        -e ROS_MASTER_URI=${ROS_MASTER_URI} \
        -v $CAMERA_DIR:/home/ros/launch \
        registry.gitlab.com/apl-ocean-engineering/bsee-oil-detection/bsee_odroid_sw/camera:${IMAGE_TAG:-latest} \
        roslaunch /home/ros/launch/bsee_camera_nodelet.launch \
                run_web_server:=true \
                web_server_port:=9080 \
                run_hdr_image_proc:=$HDR_IMAGE_PROC \
                camera_namespace:=${CAMERA_NAMESPACE} \
                camera_config_file:=/home/ros/launch/${CAMERA}_params.yaml
