#!/bin/bash
#
# Recursively rebuilds arena_camera_ros using _local source_
# and then the bsee_camera docker images
#
# Users the image_tag "local" unless the env variable
# "IMAGE_TAG" is provided:
#
#   IMAGE_TAG=test_image ./rebuild_docker_images_from_local.sh
#

IMAGE_TAG=${IMAGE_TAG:-local}

BSEE_DIR=`pwd`

if [ -d ~/arena_camera_ros/.docker ]; then
    cd ~/arena_camera_ros/.docker
    IMAGE_TAG=$IMAGE_TAG docker compose build build_local
fi

cd $BSEE_DIR
IMAGE_TAG=$IMAGE_TAG docker compose build bsee_camera
