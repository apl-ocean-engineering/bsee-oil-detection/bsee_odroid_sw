#! /usr/bin/env bash

# Script that updates default remotes from gitlab to the virtual machine,
# making it easier to work with vcs for updating code/config on the vehicle
# computers.

# Run this ONCE from ~/remus_ws/src after a new backseat computer has been provisioned

# This doesn't work if the branch isn't main: so for apl_msgs and genmypy.

for dd in `ls .`; do
    if [ ! -d ${dd} ]; then
        echo "Skipping ${dd} -- not a directory";
	continue;
    fi
    echo "updating remotes for ${dd}";
    cd ${dd};
        git remote rename origin gitlab
	git remote add vm ssh://romulus@192.168.1.160/home/romulus/remus_ws/src/${dd}
	git fetch vm
	git branch -u vm/main main
    cd ..
done
