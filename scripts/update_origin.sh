#! /usr/bin/env bash

# Script that updates the `origin` remote to the remote name
# specified on the command line
#
# Always checks if the "upstream" remote exists, if not, sets it
# from origin.
#
# Will automatically configure the "laptop" remote if that is the
# target branch.

for dd in `ls .`; do
    if [ ! -d ${dd} ]; then
        echo "Skipping ${dd} -- not a directory";
	continue;
    fi
    echo "updating remotes for ${dd}";
    cd ${dd};

	UPSTREAM_URL=`git remote get-url upstream`
	if [ "$UPSTREAM_URL" == "" ]; then
		ORIGIN_URL=`git remote get-url origin`
		echo "Setting upstream to $ORIGIN_URL"
		git remote add upstream $ORIGIN_URL
	fi

	REMOTE_URL=`git remote get-url $1`
	if [ "$REMOTE_URL" == "" ]; then
	  if [ "$1" == "laptop" ]; then
	  	git remote add laptop ssh://git@bluerov2:2022/${dd}.git
	  else
		echo "Don't know how to create remote \"$1\""
		continue
	  fi
	fi

        git remote set-url origin $REMOTE_URL
    cd ..
done
