#! /usr/bin/env bash

# Script that adds remotes for the vehicles, making it easier to pull any changes
# from vehicles to the virtual machine.

for dd in `ls .`; do
    if [ ! -d ${dd} ]; then
        echo "Skipping ${dd} -- not a directory";
    continue;
    fi
    echo "updating remotes for ${dd}";
    cd ${dd};
    git remote add legacy ssh://romulus@192.168.1.162/home/romulus/remus_ws/src/${dd}
    git remote add ngr ssh://romulus@192.168.1.161/home/romulus/remus_ws/src/${dd}
    git remote add laura git@gitlab-as-laura:apl-ocean-engineering/mcneil-auv-lab/${dd}
    cd ..
done
