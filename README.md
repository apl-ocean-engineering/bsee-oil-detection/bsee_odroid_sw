# bsee_odroid_sw

ROS Meta-package for flight software for the Odroid BSEE data collection pod.

Assumes the "ground has been prepared" using the [Odroid bootstrap scripts](https://gitlab.com/apl-ocean-engineering/bsee-oil-detection/bsee-odroid-bootstrap) in Ansible.  Those scripts will install and build a copy of this repo at `~bsee/bsee_ws/` by default.

To install packages & dependencies:

~~~bash
cd ~/bsee_ws/src
vcs import --input=bsee_odroid_s/bsee_odroid_sw.repos .
cd ~/bsee_ws
rosdep install --from-paths src --ignore-src
~~~

To run all vehicle nodes
~~~bash
1$ roscore
2$ roslaunch bsee_odroid_sw bsee.launch mission:=mission_name
~~~

### calib

Contains calibration data for different instruments; convention is that date and serial number should be included in the filename.

### docker

Dockerfile and documentation required to create docker images for the Gitlab CI pipeline.


# LICENSE

This package is released under the [BSD 3-Clause License](LICENSE) and based heavily on Laura Lindzey's [Remus Configuration](https://gitlab.com/apl-ocean-engineering/mcneil-auv-lab/mcneil_remus_config) package.
