Docker file for testing all repos in the mcneil-auv-lab organization

Various `.gitlab-ci.yml` files wll depend on the `lindzey/mcneil_remus_gitlab_ci` image.

Since we do development both on the ODroid (ARM64) and on various
laptops (AMD64), we need to build a multiarchitecture image.
This capability is supported via the `buildx` driver in newer versions of
docker (v23+).

Install docker: https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

Set up docker:
~~~bash
docker run --privileged --rm tonistiigi/binfmt --install all
docker buildx create --use
~~~

Then, generate the image and make it available:
~~~bash
docker login -u lindzey
docker buildx build --platform linux/amd64,linux/arm64/v8 -t lindzey/mcneil_remus_gitlab_ci . --push
~~~

Repos in the mcneil-auv-lab organization will typically have two CI targets: one for pre-commit hooks, and one for build/test.
It is possible to run them locally before pushing changes (to avoid commit churn while trying to debug why CI is failing).
~~~
gitlab-runner exec docker pre-commit-hooks
gitlab-runner exec docker catkin-build-test
~~~
