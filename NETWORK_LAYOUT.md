# Network Layout

## ROV Configuration

The BSEE Odroid connects to Raven through the Jxx "Trisect" camera port (Subconn DLPBH13F).  This provides 24V power and gigabit ethernet to all instruments on the sled.  The power input is switched via the "Trisect" circuit in the VIB.  Note that the power circuit is **off** by default, and must be manually enabled any time the vehicle is started.

Given a recent failure of the Raven router, the current network configuration is an unholy merger of the Raven and BSEE networks, as shown below:

![network diagram](figures/bsee_network.drawio.svg)

The router serves both the "normal" Raven endpoints on the `10.10.10.*` subnet; and the BSEE parts on the `10.10.20.*` subnet.    BSEE addresses are as follows, all hostnames are defined in DNS on the router and should resolve for any system connected to the router via ethernet or wireless.

| IP Address | Hostname | Device |
|------------|----------|--------|
| 10.147.17.60 | | BSEE router on OPAL Zerotier.  |
| 10.10.20.1 | router.bsee.apl.uw.edu | BSEE router |
| 10.10.10.1 | router.raven.apl.uw.edu | (also) BSEE router |
| 10.10.20.10 | bluerov2.bsee.apl.uw.edu, laptop.bsee.apl.uw.edu | Sled laptop "bluerov2" **on wifi** |
| 10.10.20.11 | | Sled laptop "bluerov2" **on ethernet** |
| 10.10.20.20 | odroid.bsee.apl.uw.edu, bsee1.bsee.apl.uw.edu | Odroid on the BSEE sled |
| 10.10.20.21 | adcp.bsee.apl.uw.edu, nortek.bsee.apl.uw.edu | Nortek ADCP |

Please note I've broken ethernet on BlueROV somehow, so Wifi is currently more reliable TBD.

### Client wifi

The BSEE router offers a client Wifi connection, similar to Raven.  It uses the SSID `UnleashTheRaven`

### Client configuration

The "default" piloting user is `bsee` on the laptop `bluerov2`.  From there you can ssh as `bsee@odroid` to get to the odroid



## AUV Configuration

All of above, TBD


| IP Address | Hostname | Device |
|------------|----------|--------|
| 172.31.12.168 | | Odroid on Aaron's bench (should be accessible by Zerotier) |
