<?xml version="1.0"?>

<!-- Launchfile for the Raven "sled" configuration.
-->
<launch>
  <arg name="vehicle_name" default="raven"/>

  <!-- If rosbag is started automatically (do_record = True, the default),
       "mission" must be specified -->
  <arg name="mission"/>
  <arg name="do_record" default="false"/>

  <arg name="which_camera" default="phoenix"/>

  <!-- Switches to selectively enable/disable subsystems -->
  <arg name="enable_lights" default="true"/>
  <arg name="enable_uvilux" default="true"/>
  <arg name="enable_par" default="true"/>
  <arg name="enable_camera" default="true"/>
  <arg name="enable_i2c" default="true"/>

  <arg name="on_raven" default="true"/>
  <arg name="namespace" default="raven" />

  <group ns="$(arg namespace)" >

  <node name="uvilux" pkg="chelsea_uvilux_driver" type="chelsea_uvilux_driver"
      required="true"
      output="screen" if="$(arg enable_uvilux)">
    <param name="port" type="string" value="/dev/ttyS1" />
    <param name="frame_id" type="string" value="bsee"/>
  </node>

  <node name="par" pkg="biospherical_par_driver" type="biospherical_qsp_par_driver"
      required="true"
      output="screen" if="$(arg enable_par)">
    <param name="port" type="string" value="/dev/ttyS2" />
    <param name="frame_id" type="string" value="bsee"/>
  </node>

  <node name="sealite" pkg="dspl_sealite_driver_ros" type="dspl_sealite_driver"
        output="screen" if="$(arg enable_lights)">
      <param name="port" value="socket://10.10.10.135:1000/" if="$(arg on_raven)" />
      <param name="port" value="/dev/ttyUSB0" unless="$(arg on_raven)"/>

    <remap from="sealite_status" to="lights/status" />

    <!-- n.b. I don't know if this mapping is correct re bow/stern right now -->
    <rosparam param="lights" >
      down: 2
      right: 3
    </rosparam>
    <param name="poll_interval_sec" type="int" value="1" />
  </node>

  <node name="power_manager" pkg="power_manager" type="power_manager">
    <rosparam>
      <!-- BSEE Version of the board has 5 populated power channels:
           * Uvilux       exp header 16, wiringpi 4
           * MPS PAR           "   " 18, wiringpi 5
           * Camera (at 24V)       " 29, wiringpi 21  (not currently used)
           * Camera (at 12V)       " 11, wiringpi 0
           * Blueview          "   " 13, wiringpi 2
           * ADCP              "   " 31, wiringpi 22
           -->

      <!-- Life is simpler if there are no spaces in channel names -->
      channel_names: ["Uvilux", "MPS_PAR", "Camera", "Blueview", "ADCP"]
      <!-- Physical pins are [11, 16, 18, 13, 29, 31] -->
      wiringpi_pins: [4, 5, 0, 2, 22]
      on_is_high: [false, false, false, false, false]

      <!-- Nothing on by default -->
      initial_state: [false, false, false, false, false]
    </rosparam>
    <param name="rate_hz" type="double" value="10"/>
  </node>

  <node name="leak_detector" pkg="leak_detector" type="leak_detector" required="true">
    <param name="leak_is_high" type="bool" value="true"/>
    <param name="wiringpi_pin" type="int" value="26"/>
    <param name="rate_hz" type="double" value="10"/>
  </node>

  <node name="bsee_i2c_sensor_driver" pkg="bsee_i2c_sensor_driver"
        type="bsee_i2c_sensor_driver" if="$(arg enable_i2c)"
        required="true">
    <param name="bme_rate" type="double" value="1"/>
    <param name="ina_rate" type="double" value="10"/>

    <param name="frame_id" type="string" value="bsee"/>

    <param name="bme280_i2c_address" type="string" value="0x77"/>
    <param name="ina260_i2c_address" type="string" value="0x45"/>
  </node>


  <env name="CAMERA_NAMESPACE" value="raven/phoenix" />
  <node name="camera_launcher" pkg="bsee_odroid_sw" type="start_camera_in_docker.sh"
      args="$(arg which_camera)"
      required="true"
      output="screen" if="$(arg enable_camera)"/>

  </group>

  <node if="$(arg do_record)" required="true" output="screen"
        name="record" pkg="bsee_odroid_sw"
        type="record" args="--params --auto-date --mission $(arg mission) --suffix core" />
</launch>
