# Notes from Nortek DVL

The Nortek DVL on Raven is at `10.10.10.132`.   The following DNS names are also defined:

* `dvl.raven.apl.uw.edu`
* `nortek.raven.apl.uw.edu`

The Nortek contains an integrated web server which allows configuration of the unit, data preview, and download of recorded data.  It also contains a copy of the DVL documentation (in PDF form).

# Communicating with the DVL

On Raven, the DVL only has an ethernet connection (no serial connection).   The network interfaces include:

* Port 9000 is a telnet-protocol ASCII interface (require username / password authentication)
* Port 9001 is a raw (binary) interface (requires username / password authentication)
* Port 9002 is a binary data only channel (no input accepted)
* Port 9004 is an ASCII data only channel (no input accepted).
* Port 9010/9011 - Additional Output Data Format. See chapter SETAODF.

The default username is "nortek" with a blank password.

I believe OpenSea listens on port 9002 (listen-only binary data).   We have not confirmed if it changes the unit configuration at startup (on a different socket), but believe it does **not** and simply uses the data published to it.

# Setting transmit power to zero

The DVL only measures pressure when running, so we only get pressure measurements when the unit is "pinging."  However, we can set the transmit power to zero to get pressure measurements without putting any sound in the water.

To do so:

```sh
> telnet dvl 9000
```

Username "nortek"
Password "<enter>" (no password)

```sh
<ctrl-C>
wait for "CONFIRM\nOK"

MC               #enter command mode)

SETBT,PLMODE="USER"   # Use user-configured powe (defaults to "MAX")
SETBT,PL=-100    # Set bottom track power to -100 (no transmit)
SETDVL,SR=8      # Set 8Hz sampling rate

GETBT            # Get bottom strack params to confirm

START           # DVL back to running mode

ctrl-], q       # quite telnet session
```
